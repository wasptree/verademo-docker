# Verademo Apps in Docker  - Including ISM
  
## What is this about  
This a documentation how to run the Verademo Web App (https://gitlab.com/verademo-app/verademo-web), the Verademo API (https://gitlab.com/verademo-app/verademo-api) and the corresponding database for both of them in a dockerized environment.  
  
## How to run    
It makes use of `docker-compose`that lets you run multiple docker containers at once, all on the same network able to interact which each other. The main docker-compose.yml file is found on the root directory of this repository and will start the 3 metioned docker containers.
  
You need to first generate a token and API key for the ISM. Please see https://gitlab.com/wasptree/veracode-ism-container for how to do that. 
Place the token and key inside a file called .env , for use with the included docker-compose file.

`docker-compose -f docker-compose.yml up`  
to start all 3 containers and  
`docker-compose -f docker-compose.yml down -v`  
to stop all 3 containers.  
  
There is no need to download the base images upfront, the `docker-compose` process will make sure they will be downloaded.  
  
If you want to fully wipe everything and restart from scratch, make sure to also delete the corresponding docker images with `docker image rm IMAGE-ID`.  
  
## The first run  
On the first run you should make sure to rest the database. This will either reset what is already on the database or add all required data to the database. The reset function can be found on the web app behind the reset button.  
<img src="https://gitlab.com/verademo-app/verademo-docker/-/raw/main/pictures/db_reset.png" />   
This also prepares the databased to be used by the API.  

## License  
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](license)  
